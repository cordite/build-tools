# Build Tools

<h3><p><bold><i>'I want a docker build with everything in it'</i></bold></p></h3>

This is a docker-in-docker image that presents a common set of tools used by various Cordite projects for build and testing in CI environments. It's setup for Gitlab, but can as easily be used for other CI/CD services.

Notable packages:

1. Java 1.8.0
2. Maven 3.6.3
3. NodeJS 12.18.4
4. nvm 0.36.0
5. Docker 19.03.13
6. Docker-in-Docker 18.09.6
7. Docker Compose 1.27.4
8. Python & `pip` 2.7, 3.7, 3.8
9. GCloud (gsutils, kubectl, beta features etc) 314.0.0
  
# Instructions

## Localhost

Setup a network:

```
docker network create my-dind-network
```

Create a directory for the DinD certs. In this example, running on a Linux machine, we use `/var/tmp/dind`

```
MY_DIND_CERTS=/var/tmp/dind
```

Start the DinD daemon:

```
docker run --privileged --name some-docker -d \
    --network my-dind-network --network-alias docker \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v ${MY_DIND_CERTS}:/certs/ca \
    -v ${MY_DIND_CERTS}/client:/certs/client \
    docker:dind
```

Now start the image:

```
docker run --rm --network my-dind-network \
    -e DOCKER_TLS_CERTDIR=/certs \
    -v ${MY_DIND_CERTS}/client:/certs/client:ro \
    -it cordite/build-tools:local
```

## Use within Gitlab 

`.gitlab-ci.yml` example:

```
 image: cordite/build-tools:<tag>

 variables:
   DOCKER_HOST: tcp://docker:2375/
   DOCKER_DRIVER: overlay2

 services:
   - docker:dind

 before_script:
   - docker info

 build:
   stage: build
   script:
     - ...
```

# Build-tools as a GITLAB Runner

## So you want a runner for your project?

This repo provides an image with all the build-tools required for your project. If it doesn't then update this repo. The image also includes the `gitlab-runner` binary. There is a script `gitlab-runner-run` which will register the runner if the `GITLAB_URL` and `GITLAB_TOKEN` variables are set. These variables values can be found in your projects CI/CD settings. In the CI/CD project in GCP there is a [VM instance template](https://console.cloud.google.com/compute/instanceTemplates/details/gitlab-runner-build-tools?project=cicd-chorum&organizationId=705109726108) that provides example settings for creating a new runner.

## Runner pool

In GCP there is an [instance pool](https://console.cloud.google.com/compute/instanceGroups/details/us-east1-b/gitlab-runner-build-tools?project=cicd-chorum&organizationId=705109726108) `gitlab-runner-build-tools` which provides a pool of runners for [datp gitlab projects](https://gitlab.com/groups/datp/). This pool will auto-scale based on CPU usage. New runners will be added to a maximum of 5 as CPU usage goes over 50%.

## gitlab-ci.yaml

There is no need to use gitlab-ci caching. Caching is all done locally. You should not use `image:` tag as this runner is a `shell` runner wrt gitlab. You need to set jobs to have 'build-tools' tag to be picked up by this runner.

## To do

- [ ] The runner will re-register on each reboot/restart. It should not re-register
- [ ] runner should register itself as `docker` runner too.
- [ ] Concurrency should be set to 2 in order to trigger auto-scaling on GCP
- [ ] Runner clears cache on restart. Not sure if this is preferred behaviour but provides ability to clear cache easily and frequently.

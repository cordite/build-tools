#!/bin/bash
set -e

if [ -z "$GITLAB_URL" ] || [ -z "$GITLAB_TOKEN" ]; then 
    echo "GITLAB_URL or GITLAB_TOKEN not set. Not registering runner"; 
    exit 1
else
    echo "GITLAB_URL=${GITLAB_URL}. GITLAB_TOKEN=${GITLAB_TOKEN}. Starting build-tools runner";
    # check if runner registered
    /usr/local/bin/gitlab-runner \
        register -n \
        --name "${HOSTNAME}-build-tools" \
        --executor shell \
        --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
        --docker-volumes /home/gitlab-runner/builds:/builds \
        --url $GITLAB_URL \
        --registration-token $GITLAB_TOKEN \
        --tag-list build-tools \
        --run-untagged false \
        --locked false
    chmod 666 /var/run/docker.sock
    /usr/local/bin/gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner
fi

FROM amazonlinux
LABEL MAINTAINER=@dazraf


ARG JAVA_VERSION=1.8.0
ARG MAVEN_VERSION=3.6.3
ARG NODEJS_VERSION=12.18.4
ARG DOCKER_CHANNEL=stable
ARG DOCKER_VERSION=19.03.13
ARG DOCKER_COMPOSE_VERSION=1.27.4
ARG GCLOUD_VERSION=314.0.0

LABEL   name="cordite/build-tools" \
	description="OpenJDK $JAVA_VERSION Maven $MAVEN_VERSION NodeJS $NODEJS_VERSION and Docker in Docker!" \
	maintainer="dazraf" \
	url="https://gitlab.com/cordite/build-tools"


RUN yum update -y && yum install -y amazon-linux-extras && amazon-linux-extras enable python3.8
RUN	yum install -y git bash zsh which vim nano wget tar ca-certificates less gzip python3.8 python-devel py-piplibffi-dev openssl-dev gcc libc-dev make hostname && \
	wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 && \
	chmod +x /usr/local/bin/gitlab-runner 
RUN yum groupinstall -y "Development Tools"

# Docker
RUN set -eux; \
	\
	apkArch="$(arch)"; \
	case "$apkArch" in \
	x86_64) dockerArch='x86_64' ;; \
	armhf) dockerArch='armel' ;; \
	aarch64) dockerArch='aarch64' ;; \
	ppc64le) dockerArch='ppc64le' ;; \
	s390x) dockerArch='s390x' ;; \
	*) echo >&2 "error: unsupported architecture ($apkArch)"; exit 1 ;;\
	esac; \
	\
	if ! wget -O docker.tgz "https://download.docker.com/linux/static/${DOCKER_CHANNEL}/${dockerArch}/docker-${DOCKER_VERSION}.tgz"; then \
	echo >&2 "error: failed to download 'docker-${DOCKER_VERSION}' from '${DOCKER_CHANNEL}' for '${dockerArch}'"; \
	exit 1; \
	fi; \
	\
	tar --extract \
	--file docker.tgz \
	--strip-components 1 \
	--directory /usr/local/bin/ \
	; \
	rm docker.tgz; \
	\
	dockerd --version; \
	docker --version


RUN	curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
RUN chmod a+x /usr/local/bin/docker-compose

RUN yum install -y java-$JAVA_VERSION-openjdk java-$JAVA_VERSION-openjdk-devel
RUN curl -fsSL https://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | tar xzf - -C /usr/share
RUN mv /usr/share/apache-maven-$MAVEN_VERSION /usr/share/maven 
RUN ln -s /usr/share/maven/bin/mvn /usr/bin/mvn 
RUN set -eux && \
	useradd --create-home --no-log-init --shell /bin/zsh gitlab-runner \
	&& echo 'gitlab-runner:gitlab-runner' | chpasswd \
	&& groupadd docker \
	&& usermod -aG docker gitlab-runner \
	&& usermod -aG root gitlab-runner  \
	&& usermod -aG input gitlab-runner \
	&& yum clean all \
	&& rm -rf /var/cache/yum

# gcloud and kubectl
RUN wget -q https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-$GCLOUD_VERSION-linux-x86_64.tar.gz && \
	tar zxvf google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz && \
	mv google-cloud-sdk /usr/local/share/
ENV PATH $PATH:/usr/local/share/google-cloud-sdk/bin
RUN /usr/local/share/google-cloud-sdk/bin/gcloud config set disable_usage_reporting true
RUN /usr/local/share/google-cloud-sdk/bin/gcloud components install kubectl
RUN gcloud components install beta --quiet

# oh-my-zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

ENV JAVA_HOME /usr/lib/jvm/java
ENV MAVEN_HOME /usr/share/maven

# oh-my-zsh
RUN touch /root/.zshrc
RUN git clone https://github.com/lukechilds/zsh-nvm ~/.oh-my-zsh/custom/plugins/zsh-nvm
RUN mv /root/.zshrc /root/.zshrc.original
RUN echo 'export ZSH="/root/.oh-my-zsh"' >> /root/.zshrc
RUN echo 'ZSH_THEME="robbyrussell"' >> /root/.zshrc
RUN echo 'plugins=(git zsh-nvm docker)' >> /root/.zshrc
RUN echo 'source $ZSH/oh-my-zsh.sh' >> /root/.zshrc

# # NodeJS
RUN /bin/zsh -c  "$(curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh)"
RUN /bin/zsh -c "source /root/.zshrc && nvm install ${NODEJS_VERSION}"

WORKDIR /home/gitlab-runner

COPY modprobe.sh /usr/local/bin/modprobe
COPY docker-entrypoint.sh /usr/local/bin/
COPY gitlab-runner-run.sh /usr/local/bin/gitlab-runner-run

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["zsh"]






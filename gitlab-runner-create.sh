#! /bin/bash

GITLAB_URL=$1
GITLAB_TOKEN_1=$2
GITLAB_TOKEN_2=$3

set -e

# pre-req
apt-get update
apt-get install -y software-properties-common
apt-get install -y curl wget apt-transport-https ca-certificates
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get update

# install packages
apt-get install -y \
    gitlab-runner \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    docker-compose \
    openjdk-8-jdk \
    maven \
    nodejs \
    gnupg2 \
    pass

# post install config
update-java-alternatives --set java-1.8.0-openjdk-amd64
echo '{ "registry-mirrors": ["https://mirror.gcr.io"]}' > /etc/docker/daemon.json
service docker restart
docker system info
chmod 666 /var/run/docker.sock
usermod -aG docker gitlab-runner

if [ -z "$GITLAB_TOKEN_1" ]
then
echo "no gitlab token found. gitlab runner not being registered"
else
echo "setting up gitlab runner shell executor for token ${GITLAB_TOKEN_1}"
gitlab-runner \
    register -n \
    --name "${HOSTNAME}-shell" \
    --executor shell \
    --shell bash \
    --limit 1 \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-volumes /home/gitlab-runner/builds:/builds \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN_1 \
    --tag-list build-tools \
    --run-untagged="false" \
    --locked="false"
echo "setting up gitlab runner docker executor for token ${GITLAB_TOKEN_1}"
gitlab-runner \
    register -n \
    --name "${HOSTNAME}-docker" \
    --executor docker \
    --limit 4 \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-image cordite/build-tools:latest \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN_1 \
    --tag-list docker \
    --run-untagged="false" \
    --locked="false"
gitlab-runner start
fi

if [ -z "$GITLAB_TOKEN_2" ]
then
echo "no second gitlab token found. gitlab runner not being registered"
else
echo "setting up gitlab runner shell executor for token ${GITLAB_TOKEN_2}"
gitlab-runner \
    register -n \
    --name "${HOSTNAME}-shell" \
    --executor shell \
    --shell bash \
    --limit 1 \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-volumes /home/gitlab-runner/builds:/builds \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN_2 \
    --tag-list build-tools \
    --run-untagged="false" \
    --locked="false"
echo "setting up gitlab runner docker executor for token ${GITLAB_TOKEN_2}"
gitlab-runner \
    register -n \
    --name "${HOSTNAME}-docker" \
    --executor docker \
    --limit 4 \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-image cordite/build-tools:latest \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN_2 \
    --tag-list docker \
    --run-untagged="false" \
    --locked="false"
gitlab-runner start
fi